## Exploratory tests
Target: auto.ru

## Cases

### №1 Automated

| №   | What's done                                                                         | Status |
| --- | ----------------------------------------------------------------------------------- | ------ |
| 1   | Open https://auto.ru and assert that **Авто.ру** is in the window title             | OK     |
| 2   | Wait until the ad pop-up window shows up                                            | OK     |
| 3   | Click on the **Сдам позже** button to close the pop-up                              | OK     |
| 4   | Click on the **Все марки** button to see all listed car brands                      | OK     |
| 5   | Assert that **Audi**, **Volkswagen**, and **Porsche** are listed                    | OK     |
| 6   | Click on the **Porsche** button to find listing of this brands                      | OK     |
| 7   | Iterate over all listings and assert that all of them have **Porsche** in the title | OK     |

### №2

| №   | What's done                                                             | Status |
| --- | ----------------------------------------------------------------------- | ------ |
| 1   | Open https://auto.ru and assert that **Авто.ру** is in the window title | OK     |
| 2   | Wait until the ad pop-up window shows up                                | OK     |
| 3   | Click on the **Сдам позже** button to close the pop-up                  | OK     |
| 4   | Choose the region to be **Тольятти + 0 км**                             | OK     |
| 5   | Assert that **LADA** brand has the most listings                        | OK     |

### №3


| №   | What's done                                                             | Status |
| --- | ----------------------------------------------------------------------- | ------ |
| 1   | Open https://auto.ru and assert that **Авто.ру** is in the window title | OK     |
| 2   | Wait until the ad pop-up window shows up                                | OK     |
| 3   | Click on the **Сдам позже** button to close the pop-up                  | OK     |
| 4   | Click on the **Я продаю** button                                        | OK     |
| 5   | Click on the **Добавить объявление** button                             | OK     |
| 6   | Assert that **Продайте свой автомобиль** is written on the page         | OK     |
| 7   | Select **Toyota** from the given options                                | OK     |
| 8   | Select **Avensis** from the given options                               | OK     |
| 9   | Click on the **Сбросить всё** button                                    | OK     |
| 10  | Assert that the confirmation window showed up                           | OK     |
| 11  | Confirm the action                                                      | OK     |
| 12  | Assert that **Продайте свой автомобиль** is written on the page         | OK     |