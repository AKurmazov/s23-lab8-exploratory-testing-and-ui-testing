from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

options = Options()
options.add_experimental_option("detach", True)

driver = webdriver.Chrome(options=options)
wait = WebDriverWait(driver, 30)

driver.get('https://auto.ru')
assert 'Авто.ру' in driver.title

submit_later_button = wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'AdminsMarketingPopup__secondButton-BGRBf')))
assert 'Сдам позже' in submit_later_button.text
submit_later_button.click()

all_brands_button = driver.find_element(By.CLASS_NAME, 'IndexMarks__show-all')
assert 'Все марки' in all_brands_button.text
all_brands_button.click()

all_brands = driver.find_elements(By.CLASS_NAME, 'IndexMarks__item-name')
all_brands_names = {brand_item.text for brand_item in all_brands}

GERMAN_VAG = {'Audi', 'Volkswagen', 'Porsche'}
assert GERMAN_VAG.issubset(all_brands_names)

for brand_item in all_brands:
    if brand_item.text == 'Porsche':
        brand_item.click()
        break

all_listing_titles = driver.find_elements(By.CLASS_NAME, 'ListingItemTitle__link')
all_listing_titles_texts = [listing_title_item.text for listing_title_item in all_listing_titles]

for listing_title_text in all_listing_titles_texts:
    assert 'Porsche' in listing_title_text
